import { Locale } from 'cube-ui'
export default {
    mixins: [Locale.localeMixin],
    config: {
        'app_name': '斯里蘭卡礦場',
        'domain_name': 'demo.icrown.app',
		'platform_type': 1, //1:現金模式 , 0:信用模式
		'min_bet': 1,
		'ApiUrl': '/api',
		'kfUrl': '', //客服鏈接
		'bonus_list': '', //中獎名單
		'dzp_score': 0, //大轉盤消費點數
        'cash_type':{ //1:競標, 2:利息 , 3:充值, 4提現, 5提現失敗反款, 6活動 , 7返點
            0   : 'lang.msg34',
			1   : 'lang.msg19',
			2   : 'lang.msg30',
            3   : 'lang.msg31',
            4   : 'lang.msg32',
            5   : 'lang.msg149',
            6   : 'lang.msg33',
            7   : 'lang.msg150',
        },
    }
}
