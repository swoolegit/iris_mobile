import Vue from 'vue'
import Cube from 'cube-ui'
import zhTW from './lang/zh-TW.js'
Vue.use(Cube)
Cube.Locale.use('zh-TW',zhTW)
