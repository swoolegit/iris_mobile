import moment from 'moment'
import { apiCheckLogin } from "@/api.js";
export const globalMixin = {
    methods: {
        getRechargeStatus(item){
            switch(item.Status)
            {
                case 0:
                    return this.$t('lang.msg132');
                case 1:
                    return this.$t('lang.msg133');
                case 2:
                    return this.$t('lang.msg134');
            }
        },
        getCashStatus(item){
            switch(item.Status)
            {
                case 0:
                    return this.$t('lang.msg132');
                case 1:
                    return this.$t('lang.msg21');
                case 2:
                    return this.$t('lang.msg22');
            }
        },
        checkLogin()
        {
            apiCheckLogin({
                })
                .then(response => {
                    if(!response.data.status)
                    {
                        this.error_handler(response.data);
                    }
                    if(response.data.status)
                    {
                        this.$store.dispatch('setUserInfo',response.data.data);
                    }
                    return;
                }).catch(error => {
                    console.log(error);
                    return;
                });
        },
        go2page(url){
			this.$router.push({path: url, query: {}});
        },
        reload_page()
        {
            this.$router.go(0);
        },
        error_func(errID){
            switch(errID)
            {
                case 1:
                    this.$store.dispatch('clearUserInfo');
                    return;
                case 3:
                    //var reload_issue=setInterval(this.reload_page(),3000);
                    //clearInterval(reload_issue);
                    //setTimeout(this.reload_page(),30000);
                    return;
            }
        },
        error_handler(e)
        {
            if(e.func==1)
            {
                this.toast(e.msg);
                this.$router.push("/login");
                return;
            }
            if(e.func==2)
            {
                this.toast(e.msg,null,0);
                return;
            }
            if(e.func==3)
            {
                this.toast(e.msg,{func:this.reload_page},3000);
                return;
            }
            this.toast(e.msg);
            // if(e.func)
            // {
            //     this.error_func(e.func);
            // }
            return;
        },
        dialog(type,title,content)
        {
            this.$createDialog({
                type: type,
                title: title,
                content: content,
                icon: 'cubeic-alert'
              }).show()
        },
        toast(msg, events,time=2000,type='txt'){
                if(events!=null)
                {
                    events={
                        timeout: events.func
                    };
                }
                this.$createToast({
                    txt: msg,
                    type: type,
                    time: time,
                    mask: true,
                    $events: events
                }).show();
        },
        back(){
            this.$router.back()
        },
        getMomentYDate(time){
            var dateObj = new Date(time);
            var momentObj = moment(dateObj);
            return moment(momentObj).format("YYYY-MM-DD");
        },
        getMomentDate(time){
            var dateObj = new Date(time);
            var momentObj = moment(dateObj);
            return moment(momentObj).format("MM-DD");
        },
        getMomentTime(time){
            var dateObj = new Date(time);
            var momentObj = moment(dateObj);
            return moment(momentObj).format("HH:mm");
        },
        getMomentFullDate(time)
        {
			if(time=="")
			{
				return "--";
			}
            var dateObj = new Date(time);
            var momentObj = moment(dateObj);
            return moment(momentObj).format("YYYY-MM-DD HH:mm");
        },
        getBidStatus(type){
            switch(type)
            {
                case 0:
                    return this.$t('lang.msg20');
                case 1:
                    return this.$t('lang.msg21');
                case 2:
                    return this.$t('lang.msg22');
            }
        },
		gethms(time)
		{
			let msec = 0;
			let day = 0;
			let hr = 0;
			let min = 0;
			let sec = 0;
			msec = time;
			day = parseInt(msec / 60 / 60 / 24);
			hr = parseInt(msec  / 60 / 60 % 24);
			min = parseInt(msec / 60 % 60);
			sec = parseInt(msec % 60);
			return {
				day:day > 9 ? day : '0' + day,
				hr:hr > 9 ? hr : '0' + hr,
				min:min > 9 ? min : '0' + min,
				sec:sec > 9 ? sec : '0' + sec
			}
		},
    }
}