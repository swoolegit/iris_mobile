const isProduction = process.env.NODE_ENV === 'production'
module.exports = {
    //lintOnSave:false,
    productionSourceMap: isProduction,
    // 開發時間建議不要開啟 postcss , 否則造成css修改困難
    css: {
        sourceMap: !isProduction,
        extract: isProduction,
        loaderOptions: {
            stylus: {
              'resolve url': true,
              'import': [
                './src/theme'
              ],
              modules: true,
            },
            // postcss: {
            //     plugins: [
            //         require('postcss-px2rem')({
            //             remUnit: 37.5
            //         })
            //     ]
            // }
        }
    },

    pluginOptions: {
      'cube-ui': {
        postCompile: true,
        theme: true
      },
      'style-resources-loader': {
        preProcessor: 'stylus',
        patterns: [
            require('path').resolve(__dirname, './src/style/default.module.styl'),
        ]
      }
    },

    configureWebpack: {
        devServer: {
            watchOptions: {
                ignored: /node_modules/,
            },
            disableHostCheck: true,
        },
    },

    runtimeCompiler: false
}
